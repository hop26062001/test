#ifndef _PCA9533_H_
#define _PCA9533_H_


#define const byte DEV_ADDR         = 0xC4;
#define const byte COM_SUCCESS      = 0x00;

/**
 * @brief   Register types enumeration
 */
typedef enum {
    REG_INPUT   = 0x10,     // Default :: INPUT REGISTER
    REG_PSC0    = 0x11,     // FREQUENCY PRESCALER 0
    REG_PWM0    = 0x12,     // PWM REGISTER 0
    REG_PSC1    = 0x13,     // FREQUENCY PRESCALER 1
    REG_PWM1    = 0x14,     // PWM REGISTER 1
    REG_LED     = 0x15      // LED SELECTOR
} reg_ptr_t;

/**
 * @brief   Mode led out
 */
typedef enum {
    LED_MODE_OFF  = 0,  // 00 - Output is set Hi-Z (LED off – default)
    LED_MODE_ON   = 1,  // 01 - Output is set LOW (LED on)
    LED_MODE_PWM0 = 2,  // 10 - Output blinks at PWM0 rate
    LED_MODE_PWM1 = 3   // 11 - Output blinks at PWM1 rate
} led_out_mode_t;

/**
 * @brief   PIN State
 */
typedef enum {
    IO0 = 0,
    IO1 = 2,
    IO2 = 4,
    IO3 = 6
} pin_t;

/**
 * @brief   mode on/off all pin
 */
typedef enum {
    ALL_ON   = 0x55,
    ALL_OFF  = 0x00
} mode_t;

/**
 * @brief   IO High/Low
 */
typedef enum {
    IO_LOW  = 0,
    IO_HIGH = 1
} state_t;

/**
 * @brief   PWM Add
 */
typedef enum {
    IO_PWM0  = 0x12,
    IO_PWM1  = 0x14
} pwm_t;

typedef struct tca9533_twi_config{
    uint8_t addr;
};

int PCA9533_Initcall(reg_ptr_t regPtr); 


/*
Muốn set LED ON/OFF thì có 1 thanh ghi LS0, mô tả các mode của LED tương ứng:
00 = Output is set Hi-Z (LED off – default) 
01 = Output is set LOW (LED on)
10 = Output blinks at PWM0 rate
11 = Output blinks at PWM1 rate

Muốn PWM đầu ra thì truyền vào thanh ghi PSC0 giá trị nào đó tính theo công thức
Có thể VD là 0x11



*/